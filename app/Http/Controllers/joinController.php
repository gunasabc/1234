<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class joinController extends Controller
{
    public function joinform()
    {
        return view('join');
    }

    public function join(Request $request)
    {
        $id= $request->id;
        $name= $request->name;
        $email= $request->email;
        $mobileno= $request->mobileno;
        $address= $request->address;
        $gender= $request->gender;
        $hobbies= $request->hobbies;
        $dop= $request->dop;
        $country= $request->country;



//        var_dump($request->all());die;
        DB::insert('insert into details (id, name, email, mobileno, address, gender, hobbies, dop, country) values(?,?,?,?,?,?,?,?,?)', [$id, $name, $email, $mobileno, $address, $gender, $hobbies, $dop, $country]);

        echo "Record inserted successfully.<br/>";
        echo '<a href = "/join">Click Here</a> to go back.';
    }
}