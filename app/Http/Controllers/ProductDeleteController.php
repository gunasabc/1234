<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductDeleteController extends Controller {
    public function index(){
        $users = DB::select('select * from product');
        return view('product_delete',['users'=>$users]);
    }
    public function destroy($pid) {
        DB::delete('delete from product where pid= ?',[$pid]);
        echo "Record deleted successfully.<br/>";
        echo '<a href="/delete-records">Click Here</a> to go back.';
    }
}