<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductUpdateController extends Controller {
    public function index(){
        $users = DB::select('select * from product');
        return view('Productedit',['users'=>$users]);
    }
    public function show($pid) {
        $users = DB::select('select * from product where pid = ?',[$pid]);
        return view('productupdate',['users'=>$users]);
    }
    public function edit(Request $request,$pid) {
        $name = $request->name;
        $publish = $request->publish;
        $price = $request->price;
        $address = $request->address;
        $verifyadd = $request->verifyadd;
        $category = $request->category;
        $delivery = $request->delivery;
        $description = $request->description;
        DB::update('update product set name = ?,publish = ?,price = ?,address = ?,verifyadd = ?,category = ?,delivery = ?,description = ? where pid = ?',[$name,$publish,$price,$address,$verifyadd,$category,$delivery,$description,$pid]);
        echo "Record updated successfully.<br/>";
        echo '<a href = "/edit-product">Click Here</a> to go back.';
    }
}