<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StudInsertController extends Controller
{
    public function insertform()
    {
        return view('stud_create');
    }

    public function insert(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $place = $request->place;
        $dept = $request->dept;

//        var_dump($request->all());die;
        DB::insert('insert into sankar (stu_id, name, place, dept) values(?,?,?,?)', [$id, $name, $place, $dept]);

        echo "Record inserted successfully.<br/>";
        echo '<a href = "/insert">Click Here</a> to go back.';
    }
}