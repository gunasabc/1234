<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PinsertController extends Controller
{
    public function insertform()
    {
        return view('product');
    }

    public function insert(Request $request)
    {
        $pid = $request->pid;
        $name = $request->name;
        $publish = $request->publish;
        $price = $request->price;
        $address = $request->address;
        $verifyadd = $request->verifyadd;
        $category = $request->category;
        $delivery = $request->delivery;
        $description = $request->description;
//        var_dump($request->all());die;
        DB::insert('insert into product (pid, name, publish, price, address, verifyadd, category, delivery, description ) values(?,?,?,?,?,?,?,?,?)', [$pid, $name, $publish, $price, $address, $verifyadd, $category, $delivery, $description]);

        echo "Record inserted successfully.<br/>";
        echo '<a href = "/insert">Click Here</a> to go back.';
    }
}