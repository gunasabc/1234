<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StudUpdateController extends Controller {
    public function index(){
        $users = DB::select('select * from sankar');
        return view('stud_edit_view',['users'=>$users]);
    }
    public function show($id) {
        $users = DB::select('select * from sankar where id = ?',[$id]);
        return view('stud_update',['users'=>$users]);
    }
    public function edit(Request $request,$id) {
        $stu_id = $request->stu_id;
        $name = $request->name;
        $place = $request->place;
        $dept = $request->dept;
        DB::update('update sankar set stu_id = ?,name = ?,place = ?,dept = ? where id = ?',[$stu_id,$name,$place,$dept,$id]);
        echo "Record updated successfully.<br/>";
        echo '<a href = "/edit-records">Click Here</a> to go back.';
    }
}