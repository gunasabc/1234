<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegController extends Controller
{
    public function insertform()
    {
        return view('register');
    }

    /**
     * @param Request $request
     */
    public function insert(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $email = $request->email;
        $gender = $request->gender;
        $address = $request->address;

//        var_dump($request->all());die;
        DB::insert('insert into reg (username, password, email, gender, address) values(?,?,?,?,?)', [$username, $password, $email, $gender, $address]);

        echo "<font size=\"+10\" color=\"#ff4500\">Welcome to Sankar Spinning Mill Private LTD</font>.<br/>";
        echo '<a href = "/home">Open</a>';
    }
}