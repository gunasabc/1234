<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class YesController extends Controller
{
    public function insertform()
    {
        return view('yes');
    }

    public function insert(Request $request)
    {
        $name = $request->name;
        $quantity = $request->quantity;
        $sgst = $request->sgst;
        $cgst = $request->cgst;
        $price = $request->price;
        $totalamount = $request->$quantity*$price;

//        var_dump($request->all());die;
        DB::insert('insert into no (name, quantity, sgst, cgst, price, totalamount) values(?,?,?,?,?,?)', [$name, $quantity, $sgst, $cgst, $price, $quantity*$price]);

        echo "Record inserted successfully.<br/>";
        echo '<a href = "/yes">Click Here</a> to go back.';
    }
}