<html>

<head>
    <title>View Student Records</title>
    <style>
        table, td, th {
            border: 1px solid #ddd;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            padding: 15px;
        }
    </style>
</head>

<body>
<h1 style="background-color:DodgerBlue;">Product Details</h1>
<table bgcolor="#adff2f" border = 8>
    <tr>


        <th>pid</th>
        <th>name</th>
        <th>publish</th>
        <th>price</th>
        <th>address</th>
        <th>verifyadd</th>
        <th>category</th>
        <th>delivery</th>
        <th>description</th>

    </tr>
    @foreach ($users as $user)
        <tr>

            <td>{{ $user->pid }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->publish }}</td>
            <td>{{ $user->price }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->verifyadd }}</td>
            <td>{{ $user->category }}</td>
            <td>{{ $user->delivery }}</td>
            <td>{{ $user->description }}</td>
            <td><a href = 'select/{{ $user->pid }}'>Select</a></td>
        </tr>
    @endforeach
</table>
<br>
<h1 style="background-color:DodgerBlue;">
<table>
    <tr>
            <td>  <a href = "/product"><input type="button" name="Insert" value="insert"> </a>
             <a href = "/delete-product"><input type="button" name="Delete" value="Delete"> </a>
              <a href = "/edit-product"><input type="button" name="Update" value="update"> </a>
            </td>
    </tr>
</table>
</h1>
</body>
</html>