<html>

<head>
    <title>View Student Records</title>
</head>

<body>
<table border = "1">
    <tr>

        <td>Id</td>
        <td>stu_id</td>
        <td>name</td>
        <td>place</td>
        <td>dept</td>
        <td>Edit</td>
    </tr>
    @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->stu_id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->place }}</td>
            <td>{{ $user->dept }}</td>
            <td><a href = 'delete/{{ $user->id }}'>Delete</a></td>
        </tr>
    @endforeach
</table>
<table>
    <tr>

        <td colspan='2'>
            <a href = "/view-records"><input type="submit"  value="view"> </a>
        </td>
    </tr>
</table>
</body>
</html>