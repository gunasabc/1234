<html>

<head>
    <title>View Student Records</title>
</head>
<style>
    table, td, th {
        border: 1px solid #ddd;
        text-align: left;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 15px;
    }
</style>
<body>
<h1 style="background-color:DodgerBlue;">Product Delete</h1>
<table border = "1">
    <tr>


        <th>pid</th>
        <th>name</th>
        <th>publish</th>
        <th>price</th>
        <th>address</th>
        <th>verifyadd</th>
        <th>category</th>
        <th>delivery</th>
        <th>description</th>
    </tr>
    @foreach ($users as $user)
        <tr>


            <td>{{ $user->pid }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->publish }}</td>
            <td>{{ $user->price }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->verifyadd }}</td>
            <td>{{ $user->category }}</td>
            <td>{{ $user->delivery }}</td>
            <td>{{ $user->description }}</td>

            <td><a href = 'delete/{{ $user->pid }}'>Delete</a></td>
        </tr>
    @endforeach
</table>
<table>
    <tr>

        <td colspan='2'>
            <a href = "/view-records"><input type="submit"  value="view"> </a>
        </td>
    </tr>
</table>
</body>
</html>