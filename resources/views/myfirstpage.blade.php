<!DOCTYPE html>
<html>
   
   <head>
      <title>Laravel</title>
      <link href = "https://fonts.googleapis.com/css?family=Lato:100" rel = "stylesheet" 
         type = "text/css">
      
     <style>
* {
    box-sizing: border-box;
}

body {
    margin: 0;
}

/* Style the header */
.header {
    background-color: #f1f1f1;
    padding: 50px;
    text-align: center;
}

/* Create three equal columns that floats next to each other */
.column {
    float: left;
    width: 33.33%;
    padding: 10px;
    height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Style the footer */
.footer {
    background-color: #f1f1f1;
    padding: 10px;
    text-align: center;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .column {
        width: 100%;
    }
}
</style>
   </head>
   
   <body>
   
		<div class="header">
  <h2>Header</h2>
  <h3>CSS Template using Float</h3>
  <p>In this example, we have created a header, three equal columns and a footer. On smaller screens, the columns will stack on top of each other. Resize the browser window to see the responsive effect.</p>
</div>
<div class="row">
<div class="column" style="background-color:#aaa;">Column<p>In this example, we have created a header, three equal columns and a footer. On smaller screens, the columns will stack on top of each other. Resize the browser window to see the responsive effect.</p>
  <table border="1">
  <tr><th>Name</th><th>College</th><th>Degree</th><th>Percentage</th></tr>
  <tr><td>Samuel</td><td>SHC</td><td>MCA</td><td>71%</td></tr>
  <tr><td>Krishna</td><td>SHC</td><td>MCA</td><td>73%</td></tr>
  <tr><td>Guna</td><td>SHC</td><td>MCA</td><td>74%</td></tr>
  <tr><td>Dhana</td><td>SHC</td><td>MCA</td><td>75%</td></tr>
  <tr><td>Bharath</td><td>SHC</td><td>MCA</td><td>73%</td></tr>
  </table>
  </div>
  <div class="column" style="background-color:#bbb;">Column<p>In this example, we have created a header, three equal columns and a footer. On smaller screens, the columns will stack on top of each other. Resize the browser window to see the responsive effect.</p></div>
  <div class="column" style="background-color:#ccc;">Column<p>In this example, we have created a header, three equal columns and a footer. On smaller screens, the columns will stack on top of each other. Resize the browser window to see the responsive effect.</p></div>
</div>
   <br/>
  <div class="row">
  <div class="column" style="background-color:#aaa;">second part</div>
  <div class="column" style="background-color:#bbb;">Column</div>
  <div class="column" style="background-color:#ccc;">Column</div>
  </div>


<div class="footer">
  <p>Footer</p>
</div>
      <div class = "container">
         
         <div class = "content">
            <div class = "title">Samuel V</div>
         </div>
		 
		 
			
      </div>
   </body>

</html>