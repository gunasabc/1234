<html>
<head>
    <title>View Student Records</title>
</head>

<body>

<table border = "1">
    <tr>
        <td>ID</td>
        <td>stu_id</td>
        <td>Name</td>
        <td>Place</td>
        <td>Dept</td>
        <td>Edit</td>
    </tr>
    @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->stu_id}}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->place }}</td>
            <td>{{ $user->dept }}</td>

            <td><a href = 'edit/{{ $user->id }}'>Edit</a></td>
        </tr>
    @endforeach
</table>
<table>
    <tr>

        <td colspan='2'>
            <a href = "/view-records"><input type="submit"  value="view"> </a>
        </td>
    </tr>
</table>

</body>
</html>