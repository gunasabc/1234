<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/first', function () {
    return view('first');
});

Route::get('/second', function () {
    return view('second');
});


Route::get('/home', function () {
    return view('home');
});


Route::get('/first', function () {
    return view('guna/first');
});

Route::get('/img', function () {
    return view('guna/img');
});

Route::get('/product', function () {
    return view('guna/product');
});


Route::get('Pinsert','PinsertController@insertform');
Route::post('product','PinsertController@insert');

Route::get('delete-product','ProductDeleteController@index');
Route::get('delete/{pid}','ProductDeleteController@destroy');

Route::get('select','SelectproductController@index');
Route::get('select/{pid}','SelectproductController@destroy');

Route::get('edit-product','ProductUpdateController@index');
Route::get('edit/{pid}','ProductUpdateController@show');
Route::post('edit/{pid}','ProductUpdateController@edit');


Route::get('register','RegController@insertform');
Route::post('register','RegController@insert');

Route::get('cust','CustomerController@index');


Route::get('home','LoController@insertform');
Route::post('home','LoController@insert');


Route::get('admin','AdminController@insertform');
Route::post('admin','AdminController@insert');

Route::get('yes','YesController@insertform');
Route::post('yes','YesController@insert');

Route::get('/acc', function () {
    return view('acc');
});




Route::get('insert','StudInsertController@insertform');
Route::post('create','StudInsertController@insert');

Route::get('delete-records','StudDeleteController@index');
Route::get('delete/{id}','StudDeleteController@destroy');

Route::get('edit-records','StudUpdateController@index');
Route::get('edit/{id}','StudUpdateController@show');
Route::post('edit/{id}','StudUpdateController@edit');

Route::get('view-records','StudViewController@index');

Route::get('join','joinController@joinform');
Route::post('join','joinController@join');

Route::get('/post/{id}/{name}/{password}','postsController@index');
